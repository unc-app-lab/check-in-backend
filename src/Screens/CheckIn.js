import React from 'react';
import axios from "axios";
import '../App.css';

export default class CheckIn extends React.Component {

  SubmitCheckIn(name, pid, reason) {
    //if form empty, don't submit
    if (pid === "") {
      alert("Please enter in a valid PID");
    } else if (reason === "") {
      alert("Please enter a reason for visit");
    } else {

      var today = new Date();
      var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
      var time = today.getHours() + ":" + today.getMinutes();

      const item = {
        name: name, // add name input field, make blank=false
        PID: pid,
        date: date,
        timeIn: time,
        timeOut: '00:00', // leave empty
        reason: reason,
        checkedIn: true,
        staff: ""
      };

      // need to figure out how to send authorization token in http requests 
      //axios.post('/api/checkins/', item);

      axios({ method: 'POST', url: '/api/checkins/', headers: {authorization: localStorage.token}, data: { 
        name: name,
        PID: pid,
        date: date,
        timeIn: time,
        timeOut: '00:00',
        reason: reason,
        checkedIn: true,
        staff: ""
      } });

      // navigate back to home
      this.props.history.push('');
    }
  }

  render() {
    return (
      <div class="checkin">
        <h2>Check In</h2>
        <form onSubmit={() => { this.SubmitCheckIn(document.getElementById("name").value, document.getElementById("pid").value, document.getElementById("reason").value) }}>
          <div class="textbox">
            <label>
              Name:
                  <input type="text" name="name" id="name" />
            </label>
          </div>
          <div class="textbox">
            <label>
              PID:
                  <input type="text" name="pid" id="pid" />
            </label>
            <p>(Scanner can be used to input PID)</p>
          </div>
          <div class="textbox">
            <label>
              Reason:
                  <input type="text" name="reason" id="reason" />
            </label>
          </div>
          <button class="check-in">Submit</button>
        </form>
      </div>
    );
  }
}